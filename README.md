# Pressford Assessment
## Author
Pete Johnson

## Online Demo

http://petecjohnson.eastus2.cloudapp.azure.com

The system has 2 demo users: 

    - publisher@pressford.com
    - employee@pressford.com 

Both have the password "1234"

## Intro
Because the spec says that the assessment should take no longer than 4-8 hours to complete I have based the task on the [RealWorld.IO](https://github.com/gothinkster/realworld) app (simplified Medium clone called "Conduit" - MIT license) rather than starting from scratch. I have customised the code to add the required features (and deleted some unwanted features). I have prefaced my comments with my initials "PJ" to differentiate them from the existing comments. I have not implemented a limit on the number of likes (named 'favorites' in the code), but the rest of the spec should be complete.

The back-end is [ASP.NET Core](https://github.com/gothinkster/aspnetcore-realworld-example-app) and the front-end is in [AngularJS](https://github.com/gothinkster/angularjs-realworld-example-app).


## Build & Run
Build js client from "Client" folder

- `npm install`
- `gulp`

The app can be run directly in IIS Express from Visual Studio (F5), or in an IIS site. AspNetCore 2.1 is required. The database uses SqlLite and is created the first time the app is run (in this demo it is re-created each time the app is run).

The site runs locally on http://localhost:56055/

Demo users: publisher@pressford.com / 1234  and employee@pressford.com / 1234

## API Documentation
Swagger docs are available at http://localhost:56055/swagger/index.html

## Architecture

I chose to use the Conduit demo as the basis for my submission because (apart from it having the majority of the features needed already) it adheres to some good coding practices. The ASP.NET core backend is based on Jimmy Bogard's reference architecture; [https://github.com/jbogard/ContosoUniversityCore](https://github.com/jbogard/ContosoUniversityCore)

This includes:
   - CQRS and the Mediator Pattern - helps keep the controllers thin and makes code more testable. Code is structured based on feature and I think this is more readable (separation of reads/writes into queries/commands ). The Mediatr library might be overkill for this project (as there is a one-to-one mapping between commands,querys and handlers), but it could be useful as the system grows.
   - Validation using [Fluent Validation](https://github.com/JeremySkinner/FluentValidation)
   - JWT authentication (hashed passwords stored in the db)
   - Swagger documentation (with JWT authentication). This was helpful in testing the API before I set up the front-end.
   - EF core (using SQLLite). The in-memory features are also used in the unit tests.

## Still To Do

- Implement a 'Like' limit per user.
- Rename 'Favourite' to 'Like' in the code.
- Remove redudant code - e.g. "FollowedPeople","Feed".
- Fix the user settings page - "My Articles/All Articles"

