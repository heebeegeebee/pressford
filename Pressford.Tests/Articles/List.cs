using FakeItEasy;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Pressford.Domain;
using Pressford.Infrastructure;
using System.Linq;
using System.Threading;
using Xunit;
using ArticlesList = Pressford.Features.Articles.List;

namespace Pressford.Tests.Articles
{
    public class List
    {
        readonly DbContextOptions _dbContextOptions;
        readonly ICurrentUserAccessor _currentUserAccessor;
        readonly ArticlesList.Query _query;

        public List()
        {
            _query = new ArticlesList.Query(tag: null, author: null, favorited: null, limit: null, offset: null);

            var serviceProvider = new ServiceCollection()
            .AddEntityFrameworkInMemoryDatabase()
            .BuildServiceProvider();

            _dbContextOptions = new DbContextOptionsBuilder<PressfordContext>()
                .UseInMemoryDatabase(databaseName: "testdb")
                .UseInternalServiceProvider(serviceProvider)
                .Options;

            _currentUserAccessor = A.Fake<ICurrentUserAccessor>();
            A.CallTo(() => _currentUserAccessor.GetCurrentUsername()).Returns("testuser");

        }

        /// <summary>
        /// TEST Data:
        ///         Article1 = 2 favourites
        ///         Article2 = 3 favourites
        ///         Article3 = 0 favourites
        ///         Article4 = 4 favourites (Top Article)
        /// </summary>
        [Fact]
        public void QueryHandler_GivenArticlesWithFavorites_ReturnsListOfArticlesWithTopArticleFlagged()
        {
            using (var context = new PressfordContext(_dbContextOptions))
            {
                AddArticleData(context);
                AddArticleFavourites(context);

                var queryHandler = new ArticlesList.QueryHandler(context, _currentUserAccessor);

                //ACT
                var articles = queryHandler.Handle(_query, CancellationToken.None).GetAwaiter().GetResult();

                //ASSERT
                var article4 = articles.Articles.Find(a => a.ArticleId == 4);
                Assert.True(article4.IsTopArticle);
            }
        }

        [Fact]
        public void QueryHandler_GivenArticlesWithNoFavorites_ReturnsListOfArticlesWithNoneSetAsTopArticle()
        {

            using (var context = new PressfordContext(_dbContextOptions))
            {
                AddArticleData(context);
               
                var queryHandler = new ArticlesList.QueryHandler(context, _currentUserAccessor);

                //ACT
                var articles = queryHandler.Handle(_query, CancellationToken.None).GetAwaiter().GetResult();

                //ASSERT
                var topArticles = articles.Articles.Count(a => a.IsTopArticle == true);
                Assert.Equal(0, topArticles);
            }
        }

        /// <summary>
        /// TEST Data:
        ///         Article1 = 2 favourites
        ///         Article2 = 4 favourites (Top Article)
        ///         Article3 = 0 favourites
        ///         Article4 = 4 favourites (Top Article)
        /// </summary>
        [Fact]
        public void QueryHandler_GivenTwoArticlesWithMaxFavorites_ReturnsListOfArticlesWith2TopArticlesFlagged()
        {
            using (var context = new PressfordContext(_dbContextOptions))
            {
                AddArticleData(context);
                AddArticleFavourites(context);
                context.Add(new ArticleFavorite { ArticleId = 2, PersonId = 100 });
                context.SaveChanges();

                var queryHandler = new ArticlesList.QueryHandler(context, _currentUserAccessor);

                //ACT
                var articles = queryHandler.Handle(_query, CancellationToken.None).GetAwaiter().GetResult();

                //ASSERT
                var article4 = articles.Articles.Find(a => a.ArticleId == 4);
                Assert.True(article4.IsTopArticle, "Article 4 is not marked IsTopArticle");
                var article2 = articles.Articles.Find(a => a.ArticleId == 2);
                Assert.True(article2.IsTopArticle, "Article 2 is not marked IsTopArticle");
                var topArticles = articles.Articles.Count(a => a.IsTopArticle == true);
                Assert.True(topArticles == 2, "The number of articles with IsTopArticles = true is not 2");

            }
        }

        private void AddArticleData(PressfordContext context)
        {
            context.Add(TestData.Employee(1));
            context.Add(TestData.Employee(2));
            context.Add(TestData.Employee(3));
            context.Add(TestData.Employee(4));
            context.Add(TestData.Publisher());
            context.Add(TestData.Article(1));
            context.Add(TestData.Article(2));
            context.Add(TestData.Article(3));
            context.Add(TestData.Article(4));
            context.SaveChanges();
        }

        private void AddArticleFavourites(PressfordContext context)
        {
            context.Add(new ArticleFavorite { ArticleId = 1, PersonId = 1 });
            context.Add(new ArticleFavorite { ArticleId = 1, PersonId = 2 });
            context.Add(new ArticleFavorite { ArticleId = 1, PersonId = 3 });
            context.Add(new ArticleFavorite { ArticleId = 2, PersonId = 2 });
            context.Add(new ArticleFavorite { ArticleId = 2, PersonId = 3 });
            context.Add(new ArticleFavorite { ArticleId = 2, PersonId = 4 });
            context.Add(new ArticleFavorite { ArticleId = 4, PersonId = 1 });
            context.Add(new ArticleFavorite { ArticleId = 4, PersonId = 2 });
            context.Add(new ArticleFavorite { ArticleId = 4, PersonId = 3 });
            context.Add(new ArticleFavorite { ArticleId = 4, PersonId = 4 });
            context.SaveChanges();
        }
    }
}
