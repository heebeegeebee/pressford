﻿using Pressford.Domain;
using Pressford.Infrastructure.Security;
using System;

namespace Pressford.Tests.Articles
{

    public static class TestData
    {
        public static Byte[] Salt = Guid.NewGuid().ToByteArray();
        public static string Password = "1234";

        public static Person Publisher() => new Person
        {
            PersonId = 100,
            Username = "Publisher User",
            Email = "publisher@pressford.com",
            Hash = new PasswordHasher().Hash(Password, Salt),
            Salt = Salt,
            IsPublisher = true
        };

        public static Person Employee(int id, bool isPublisher = false) => new Person
        {
            PersonId = id,
            Username = $"Employee User {id}",
            Email = $"employee{id}@pressford.com",
            Hash = new PasswordHasher().Hash(Password, Salt),
            Salt = Salt,
            IsPublisher = isPublisher
        };

        public static Article Article(int id) => new Article
        {
            ArticleId = id,
            Slug = $"article_{id}",
            Title = $"article{id}",
            Description = $"article{id}",
            Body = $"article{id}",
            AuthorId = 1,
            CreatedAt = new DateTime(2018, 1, id)
        };

    }
}

