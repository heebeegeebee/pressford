using FakeItEasy;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Pressford.Domain;
using Pressford.Infrastructure;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Xunit;
using Chart = Pressford.Features.Articles.Chart;

namespace Pressford.Tests.Articles
{
    public class ChartTests
    {
        readonly DbContextOptions _dbContextOptions;
        readonly Chart.Query _query;

        public ChartTests()
        {
            _query = new Chart.Query();

            var serviceProvider = new ServiceCollection()
            .AddEntityFrameworkInMemoryDatabase()
            .BuildServiceProvider();

            _dbContextOptions = new DbContextOptionsBuilder<PressfordContext>()
                .UseInMemoryDatabase(databaseName: "testdb")
                .EnableSensitiveDataLogging()
                .UseInternalServiceProvider(serviceProvider)
                .Options;
        }

        /// <summary>
        /// TEST Data:
        ///         Article1-5 = favourite count 0
        ///         Article6-15 = favourite count same as ID
        /// </summary>
        [Fact]
        public void QueryHandler_GivenArticlesWithFavorites_ReturnsChartData()
        {
            using (var context = new PressfordContext(_dbContextOptions))
            {
                AddArticleData(context);
                AddArticleFavourites(context);

                var queryHandler = new Chart.QueryHandler(context);

                //ACT
                var chart = queryHandler.Handle(_query, CancellationToken.None).GetAwaiter().GetResult();

                var expectedLabels = new List<string> { "article6", "article7", "article8", "article9", "article10", "article11", "article12", "article13", "article14", "article15" };
                var expectedData = new List<int> { 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
                
                //ASSERT
                foreach (var label in chart.Labels)
                {
                    Assert.True(expectedLabels.Contains(label), $"Label {label} was not in list of expected labels");
                }

                foreach (var datum in chart.Data)
                {
                    Assert.True(expectedData.Contains(datum), $"Datum {datum} was not in list of expected data");
                }
            }
        }

        [Fact]
        public void QueryHandler_GivenArticlesWithNoFavorites_ReturnsEmptyChartData()
        {

            using (var context = new PressfordContext(_dbContextOptions))
            {
                AddArticleData(context);
               
                var queryHandler = new Chart.QueryHandler(context);

                //ACT
                var chart = queryHandler.Handle(_query, CancellationToken.None).GetAwaiter().GetResult();

                //ASSERT
                Assert.Empty(chart.Labels);
            }
        }

       

        private void AddArticleData(PressfordContext context)
        {
            for(int i=1;i<15;i++)
            {
                context.Add(TestData.Employee(i));
            }
            
            for(int i = 1; i < 15; i++)
            {
                context.Add(TestData.Article(i));
            }
   
            context.SaveChanges();
        }

        private void AddArticleFavourites(PressfordContext context)
        {
            for(int i=6; i< 16; i++)
            {
                for (int j = 1; j < i+1; j++)
                {
                    context.Add(new ArticleFavorite { ArticleId = i, PersonId = j });
                }
            }
         
            context.SaveChanges();
        }
    }
}
