﻿using Pressford.Domain;
using Microsoft.EntityFrameworkCore;
using Pressford.Infrastructure.Security;

namespace Pressford.Infrastructure
{
    public class PressfordContext : DbContext
    {
        private readonly string _databaseName = Startup.DATABASE_FILE;

        public PressfordContext(DbContextOptions options) 
            : base(options)
        {
        }

        public PressfordContext(string databaseName)
        {
            _databaseName = databaseName;
        }

        public DbSet<Article> Articles { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Person> Persons { get; set; }
        //public DbSet<Tag> Tags { get; set; }
        //public DbSet<ArticleTag> ArticleTags { get; set; }
        public DbSet<ArticleFavorite> ArticleFavorites { get; set; }
        //public DbSet<FollowedPeople> FollowedPeople { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite($"Filename={_databaseName}");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<ArticleTag>(b =>
            //{
            //    b.HasKey(t => new { t.ArticleId, t.TagId });

            //    b.HasOne(pt => pt.Article)
            //    .WithMany(p => p.ArticleTags)
            //    .HasForeignKey(pt => pt.ArticleId);

            //    b.HasOne(pt => pt.Tag)
            //    .WithMany(t => t.ArticleTags)
            //    .HasForeignKey(pt => pt.TagId);
            //});

            modelBuilder.Entity<ArticleFavorite>(b =>
            {
                b.HasKey(t => new { t.ArticleId, t.PersonId });

                b.HasOne(pt => pt.Article)
                    .WithMany(p => p.ArticleFavorites)
                    .HasForeignKey(pt => pt.ArticleId);

                b.HasOne(pt => pt.Person)
                    .WithMany(t => t.ArticleFavorites)
                    .HasForeignKey(pt => pt.PersonId);
            });

            modelBuilder.Entity<FollowedPeople>(b =>
            {
                b.HasKey(t => new { t.ObserverId, t.TargetId });

                b.HasOne(pt => pt.Observer)
                    .WithMany(p => p.Followers)
                    .HasForeignKey(pt => pt.ObserverId);

                b.HasOne(pt => pt.Target)
                    .WithMany(t => t.Following)
                    .HasForeignKey(pt => pt.TargetId);
            });

            // PJ Add data for the demo
            AddSeedData(modelBuilder);
        }

        private void AddSeedData(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Person>().HasData(DemoData.Employee);
            modelBuilder.Entity<Person>().HasData(DemoData.Employee2);
            modelBuilder.Entity<Person>().HasData(DemoData.Employee3);
            modelBuilder.Entity<Person>().HasData(DemoData.Employee4);
            modelBuilder.Entity<Person>().HasData(DemoData.Publisher);
            modelBuilder.Entity<Article>().HasData(DemoData.Article1);
            modelBuilder.Entity<Article>().HasData(DemoData.Article2);
            modelBuilder.Entity<Article>().HasData(DemoData.Article3);
            modelBuilder.Entity<Article>().HasData(DemoData.Article4);
     
            modelBuilder.Entity<ArticleFavorite>().HasData(new ArticleFavorite { ArticleId = 1, PersonId = 2 });
            modelBuilder.Entity<ArticleFavorite>().HasData(new ArticleFavorite { ArticleId = 1, PersonId = 3 });
            modelBuilder.Entity<ArticleFavorite>().HasData(new ArticleFavorite { ArticleId = 2, PersonId = 2 });
            modelBuilder.Entity<ArticleFavorite>().HasData(new ArticleFavorite { ArticleId = 2, PersonId = 3 });
            modelBuilder.Entity<ArticleFavorite>().HasData(new ArticleFavorite { ArticleId = 2, PersonId = 4 });
            modelBuilder.Entity<ArticleFavorite>().HasData(new ArticleFavorite { ArticleId = 4, PersonId = 1 });
            modelBuilder.Entity<ArticleFavorite>().HasData(new ArticleFavorite { ArticleId = 4, PersonId = 2 });
            modelBuilder.Entity<ArticleFavorite>().HasData(new ArticleFavorite { ArticleId = 4, PersonId = 3 });
            modelBuilder.Entity<ArticleFavorite>().HasData(new ArticleFavorite { ArticleId = 4, PersonId = 4 });

        }
    }
}
