﻿using Pressford.Domain;
using System.Threading.Tasks;

namespace Pressford.Infrastructure.Security
{
    public interface IJwtTokenGenerator
    {
        Task<string> CreateToken(Person person);
    }
}