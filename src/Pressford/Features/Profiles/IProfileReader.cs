﻿using System.Threading.Tasks;

namespace Pressford.Features.Profiles
{
    public interface IProfileReader
    {
        Task<ProfileEnvelope> ReadProfile(string username);
    }
}