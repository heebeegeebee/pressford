using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Pressford.Infrastructure;
using Pressford.Infrastructure.Errors;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Pressford.Features.Articles
{
    public class Chart
    {
        public class Query : IRequest<ChartEnvelope>
        {
          
        }

        public class QueryHandler : IRequestHandler<Query, ChartEnvelope>
        {
            private readonly PressfordContext _context;

            public QueryHandler(PressfordContext context)
            {
                _context = context;
            }

            public Task<ChartEnvelope> Handle(Query message, CancellationToken cancellationToken)
            {
                var chartEnvelope = new ChartEnvelope();

                // PJ - Include not working for ArticleFavourites, using query syntax instead
                //var articles = _context.Articles
                //.Include(a => a.ArticleFavorites)
                //.AsNoTracking()
                //.Where(a => a.FavoritesCount > 0)
                //.OrderByDescending(a => a.FavoritesCount);

                var articles = from articlefavorites in _context.ArticleFavorites.AsNoTracking().Include("Article")
                               group articlefavorites by articlefavorites.Article into g
                               let count = g.Count()
                               orderby count descending
                               select new { Article = g.Key, Count = g.Count() };

                if (articles.Count() == 0)
                {
                    return Task.FromResult(chartEnvelope);
                }

                var top10Articles = articles.Take(10);
                chartEnvelope.Labels = top10Articles.Select(a => a.Article.Description ).ToList();
                chartEnvelope.Data = top10Articles.Select(a => a.Count).ToList();

                return Task.FromResult(chartEnvelope);
               
            }

        }
    }
}