using System.Linq;
using Pressford.Domain;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using Pressford.Infrastructure;

namespace Pressford.Features.Articles
{
    public static class ArticleExtensions
    {
        public static IQueryable<Article> GetAllData(this DbSet<Article> articles)
        {
            return articles
                .Include(x => x.Author)
                .Include(x => x.ArticleFavorites)
                //.Include(x => x.ArticleTags)
                .AsNoTracking();
        }

        public static List<Article> SetTopArticle(this List<Article> articles, int maxFavouritesCount)
        {
            if (maxFavouritesCount == 0) return articles;

            articles.ForEach(a => { if (a.FavoritesCount == maxFavouritesCount) a.IsTopArticle = true; });
            return articles;
        }

        public static int GetMaxArticleFavorites(this PressfordContext context)
        {
            // PJ calculate the max favourites for articles (non-exclusive) so we can Show this in the UI
            var articleFavorites = from favorites in context.ArticleFavorites.AsNoTracking()
                                   group favorites by favorites.ArticleId into f
                                   let count = f.Count()
                                   orderby count descending
                                   select new { ArticleId = f.Key, Count = f.Count() };

            if (articleFavorites.Count() == 0) return 0;

            return articleFavorites.First().Count;
        }
    }
}