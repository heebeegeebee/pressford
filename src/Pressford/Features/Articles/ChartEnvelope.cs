using Pressford.Domain;
using System.Collections.Generic;

namespace Pressford.Features.Articles
{
    public class ChartEnvelope
    {
        public ChartEnvelope()
        {
            Labels = new List<string>();
            Data = new List<int>();
            Series = new List<string>() { "Articles" };
        }

        public List<string> Labels { get; set; } 
        public List<string> Series { get; set; } 
        public List<int> Data { get; set; }
    }
}