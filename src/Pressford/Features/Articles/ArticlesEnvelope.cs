using System.Collections.Generic;
using Pressford.Domain;

namespace Pressford.Features.Articles
{
    public class ArticlesEnvelope
    {
        public List<Article> Articles { get; set; }

        public int ArticlesCount { get; set; }
    }
}