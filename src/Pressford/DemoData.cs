﻿using Pressford.Domain;
using Pressford.Infrastructure.Security;
using System;
using System.Collections.Generic;

namespace Pressford
{
    // PJ Demo Data for POC
    public static class DemoData
    {
        public static Byte[] Salt = Guid.NewGuid().ToByteArray();
        public static string Password = "1234";
        public static Person Publisher = new Person
        {
            PersonId = 1,
            Username = "Publisher User",
            Email = "publisher@pressford.com",
            Hash = new PasswordHasher().Hash(Password, Salt),
            Salt = Salt,
            IsPublisher = true
        };

        public static Person Employee = new Person
        {
            PersonId = 2,
            Username = "Employee User",
            Email = "employee@pressford.com",
            Hash = new PasswordHasher().Hash(Password, Salt),
            Salt = Salt,
            IsPublisher = false
        };

        public static Person Employee2 = new Person
        {
            PersonId = 3,
            Username = "Employee User 2",
            Email = "employee2@pressford.com",
            Hash = new PasswordHasher().Hash(Password, Salt),
            Salt = Salt,
            IsPublisher = false
        };

        public static Person Employee3 = new Person
        {
            PersonId = 4,
            Username = "Employee User 3",
            Email = "employee3@pressford.com",
            Hash = new PasswordHasher().Hash(Password, Salt),
            Salt = Salt,
            IsPublisher = false
        };

        public static Person Employee4 = new Person
        {
            PersonId = 5,
            Username = "Employee User 4",
            Email = "employee4@pressford.com",
            Hash = new PasswordHasher().Hash(Password, Salt),
            Salt = Salt,
            IsPublisher = false
        };

        public static Article Article1 = new Article
        {
            ArticleId = 1,
            Slug = "example_article",
            Title = "This is an example article written by publisher@pressford.com",
            Description = "First of many articles",
            Body = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec aliquet odio nisi, interdum tincidunt quam dapibus quis. Nulla vitae erat ac nibh aliquet maximus. Ut convallis enim velit, vel varius nulla auctor et. Duis at maximus nisi. Integer dignissim, mauris eu efficitur condimentum, lacus lorem sodales sem, eu ornare sem magna ac ligula. Duis at fermentum lectus, eget posuere sapien. Nullam blandit diam est, ac tempus nulla lacinia et. Mauris semper commodo laoreet.

Fusce scelerisque convallis nisi,
            mattis feugiat massa egestas sit amet.Sed consectetur facilisis aliquet.Proin volutpat,
            libero nec placerat egestas,
            sapien metus feugiat magna,
            vel congue magna lectus a nunc.Nulla facilisi.Suspendisse mollis maximus urna,
            nec consequat dui placerat non.Lorem ipsum dolor sit amet,
            consectetur adipiscing elit.Cras quis augue elementum,
            accumsan neque a,
            mattis ipsum.Fusce erat lectus,
            mattis quis tortor ut,
            tempor porttitor odio.Donec quis nunc mattis,
            tincidunt urna in,
            iaculis justo.In enim justo,
            laoreet et eros in,
            condimentum ultricies massa.

Suspendisse nec ipsum sit amet ante auctor pharetra quis a ex.Cras sodales sem feugiat luctus bibendum.Curabitur fringilla mi velit,
            ut pulvinar eros aliquam ac.Aliquam sit amet metus placerat,
            congue mauris nec,
            pellentesque est.Sed non congue nunc.Maecenas a enim magna.Maecenas dapibus mi lacus,
            vestibulum vestibulum ligula dapibus sit amet.Sed non leo sit amet mi elementum varius sed eu ex.",
            AuthorId = 1,
            CreatedAt = new DateTime(2018, 1, 1)
        };

        public static Article Article2 = new Article
        {
            ArticleId = 2,
            Slug = "article_two",
            Title = "Another article written by publisher@pressford.com",
            Description = "Second in the series",
            Body = @"Lorem ipsum dolor sit amet,
            consectetur adipiscing elit.Donec aliquet odio nisi,
            interdum tincidunt quam dapibus quis.Nulla vitae erat ac nibh aliquet maximus.Ut convallis enim velit,
            vel varius nulla auctor et.Duis at maximus nisi.Integer dignissim,
            mauris eu efficitur condimentum,
            lacus lorem sodales sem,
            eu ornare sem magna ac ligula.Duis at fermentum lectus,
            eget posuere sapien.Nullam blandit diam est,
            ac tempus nulla lacinia et.Mauris semper commodo laoreet.",
            AuthorId = 1,
            CreatedAt = new DateTime(2018, 2, 1)
        };

        public static Article Article3 = new Article
        {
            ArticleId = 3,
            Slug = "article_three",
            Title = "Third article for Pressford",
            Description = "Latest news from Pressford Consulting",
            Body = @"Lorem ipsum dolor sit amet,
            consectetur adipiscing elit.Donec aliquet odio nisi,
            interdum tincidunt quam dapibus quis.Nulla vitae erat ac nibh aliquet maximus.Ut convallis enim velit,
            vel varius nulla auctor et.Duis at maximus nisi.Integer dignissim,
            mauris eu efficitur condimentum,
            lacus lorem sodales sem,
            eu ornare sem magna ac ligula.Duis at fermentum lectus,
            eget posuere sapien.Nullam blandit diam est,
            ac tempus nulla lacinia et.Mauris semper commodo laoreet.",
            AuthorId = 1,
            CreatedAt = new DateTime(2018, 3, 1)
        };

        public static Article Article4 = new Article
        {
            ArticleId = 4,
            Slug = "fourth_article",
            Title = "Pressford Consulting Launches New Website",
            Description = "Get your latest Pressford News Online",
            Body = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec aliquet odio nisi, interdum tincidunt quam dapibus quis. Nulla vitae erat ac nibh aliquet maximus. Ut convallis enim velit, vel varius nulla auctor et. Duis at maximus nisi. Integer dignissim, mauris eu efficitur condimentum, lacus lorem sodales sem, eu ornare sem magna ac ligula. Duis at fermentum lectus, eget posuere sapien. Nullam blandit diam est, ac tempus nulla lacinia et. Mauris semper commodo laoreet.

Fusce scelerisque convallis nisi,
            mattis feugiat massa egestas sit amet.Sed consectetur facilisis aliquet.Proin volutpat,
            libero nec placerat egestas,
            sapien metus feugiat magna,
            vel congue magna lectus a nunc.Nulla facilisi.Suspendisse mollis maximus urna,
            nec consequat dui placerat non.Lorem ipsum dolor sit amet,
            consectetur adipiscing elit.Cras quis augue elementum,
            accumsan neque a,
            mattis ipsum.Fusce erat lectus,
            mattis quis tortor ut,
            tempor porttitor odio.Donec quis nunc mattis,
            tincidunt urna in,
            iaculis justo.In enim justo,
            laoreet et eros in,
            condimentum ultricies massa.",
            AuthorId = 1,
            CreatedAt = new DateTime(2018, 4, 1)
        };
    }
}
