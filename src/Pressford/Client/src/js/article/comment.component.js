class CommentCtrl {
    constructor(User) {
        'ngInject';

        this.User = User;
    }

    $onInit() {
        // PJ wait until bindings have been initialized
        if (this.User.current && this.data) {
            this.canModify = this.User.current.username === this.data.author.username;
        } else {
            this.canModify = false;
        }
    }
}

let Comment = {
  bindings: {
    data: '=',
    deleteCb: '&'
  },
  controller: CommentCtrl,
  templateUrl: 'article/comment.html'
};

export default Comment;
