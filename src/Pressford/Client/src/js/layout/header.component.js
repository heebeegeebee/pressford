class AppHeaderCtrl {
    constructor(AppConstants, User, $scope) {
        'ngInject';

        this.appName = AppConstants.appName;
        this.userService = User;
        this.currentUser = User.current;

        $scope.$watch('User.current', (newUser) => {
            this.currentUser = newUser;

            // PJ check JWT token for isPublisher property
            this.isPublisher = false;

            if (newUser && newUser.token) {
                let jwt = newUser.token;
                let jwtData = jwt.split('.')[1];
                let decodedJwtJsonData = window.atob(jwtData);
                let decodedJwtData = JSON.parse(decodedJwtJsonData);
                this.isPublisher = decodedJwtData['http://schemas.microsoft.com/ws/2008/06/identity/claims/role'] === 'Publisher';
            }
        });
    }

    logout()
    {
        console.log('logging out user');
        this.userService.logout();
    }
}

let AppHeader = {
    controller: AppHeaderCtrl,
    templateUrl: 'layout/header.html'
};

export default AppHeader;
