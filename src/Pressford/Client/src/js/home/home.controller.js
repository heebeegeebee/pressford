class HomeCtrl {
    constructor(Articles, AppConstants, $scope) {
        'ngInject';

        this.appName = AppConstants.appName;
        this._$scope = $scope;
        this.activeTab = 'Articles';

        Articles.getChartData().then((res) => {
            this.labels = res.labels;
            this.series = res.series;
            this.data = res.data;
        });
    }

    onChartClick(points, evt)
    {
        console.log('Chart Item Clicked: ' + points[0]._model.label);
    }

    tabClick(tabName)
    {
        if (tabName === 'Articles')
        {
            this.activeTab = 'Articles';
        }
        else
        {
            this.activeTab = 'Chart';
        }
    }

}

export default HomeCtrl;
