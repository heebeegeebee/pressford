const AppConstants = {
  api: 'http://localhost:56055/api',
  jwtKey: 'jwtToken',
  appName: 'Pressford',
};

export default AppConstants;
