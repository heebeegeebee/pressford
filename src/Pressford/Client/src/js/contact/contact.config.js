﻿function ContactConfig($stateProvider) {
    'ngInject';

    $stateProvider
        .state('app.contact', {
            url: '/',
            // PJ Don't need a controller as page is currently static
            //controller: 'HomeCtrl',
            //controllerAs: '$ctrl',
            templateUrl: 'contact/contact.html',
            title: 'Contact'
        });

};

export default ContactConfig;
