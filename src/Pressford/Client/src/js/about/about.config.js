﻿function AboutConfig($stateProvider) {
    'ngInject';

    $stateProvider
        .state('app.about', {
            url: '/',
            // PJ Don't need a controller as page is currently static
            //controller: 'HomeCtrl',
            //controllerAs: '$ctrl',
            templateUrl: 'about/about.html',
            title: 'About'
        });

};

export default AboutConfig;
