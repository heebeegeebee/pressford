using System.ComponentModel.DataAnnotations.Schema;

namespace Pressford.Domain
{
    public class ArticleFavorite
    {
        [ForeignKey("Article")]
        public int ArticleId { get; set; }
        public Article Article { get; set; }

        [ForeignKey("Person")]
        public int PersonId { get; set; }
        public Person Person { get; set; }
    }
}